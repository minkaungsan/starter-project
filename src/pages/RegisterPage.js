import React, { useState, useContext } from 'react'
import Grid from '@material-ui/core/Grid'
import InputGrid from '../components/InputGrid'
import TextField from '../components/TextField'
import Container from '../components/Container'
import Checkbox from '@material-ui/core/Checkbox'
import Button from '@material-ui/core/Button'
import Paper from '../components/Paper'
import Radio from '@material-ui/core/Radio'
import { useHistory } from 'react-router-dom'
import axios from '../axios'
import { GlobalContext } from '../GlobalContext'


const RegisterPage = () => {
    const { state, dispatch} = useContext(GlobalContext)

    const [name, setName] = useState({ value: '', error: null })
    const [email, setEmail] = useState({ value: '', error: null })
    const [password, setPassword] = useState({ value: '', error: null })
    const [rePassword, setRePassword] = useState({ value: '', error: null })
    const [dob, setDob] = useState('2020-07-23')
    const [gender, setGender] = useState('FEMALE')
    const [aggreement, setAggrement] = useState(false)

    const [isSubmitting, setIsSubmitting] = useState(false)

    const hasErrors = (name.error || email.error || password.error || rePassword.error || !aggreement) ? true : false

    const history = useHistory()

    const _name = event => {
        if (event.target.value === '') {
            setName({
                value: event.target.value,
                error: "Name can't be empty",
            })
        } else {
            setName({
                value: event.target.value,
                error: null,
            })
        }
    }

    const _email = event => {
        if (event.target.value === '') {
            setEmail({
                value: event.target.value,
                error: "Email can't be empty",
            })
        } else if (!(/^\w+([.-]?\w+)*@\w+([.-]?\w+)*(\.\w{2,3})+$/.test(event.target.value))) {
            setEmail({
                value: event.target.value,
                error: 'Please enter valid email'
            })
        } else {
            setEmail({
                value: event.target.value,
                error: null,
            })
        }
    }

    const _password = event => {
        if (event.target.value === '') {
            setPassword({
                value: event.target.value,
                error: "Password can't be empty",
            })
        } else {
            setPassword({
                value: event.target.value,
                error: null,
            })
        }
    }

    const _rePassword = event => {
        console.log(event.target.value, password)
        if (event.target.value === '') {
            setRePassword({
                value: event.target.value,
                error: "Please re-enter your password",
            })
        } else if (event.target.value !== password.value) {
            setRePassword({
                value: event.target.value,
                error: "Password didn't match",
            })
        } else {
            setRePassword({
                value: event.target.value,
                error: null,
            })
        }
    }


    const _register = () => {
        const formValues = {
            name: name.value,
            email: email.value,
            password: password.value,
            dob,
            gender,
        }

        console.log(JSON.stringify(formValues))
        setIsSubmitting(true)
        //... send to backend server
        axios.post('/users', formValues)
            .then(res => {
                localStorage.setItem('user', JSON.stringify(res.data))
                dispatch({ type: 'update-user', payload: res.data})
                setIsSubmitting(false)
                history.push('/')
            })
          
    }

    return (
        <Container>
            <Paper>
                <Grid
                    container
                    direction="column"
                    alignItems="stretch"
                >
                    <Grid item>
                        <h3 style={{ textAlign: 'center', width: '100%' }}>Register an account</h3>
                    </Grid>
                    <InputGrid item>
                        <TextField
                            disabled={isSubmitting}
                            error={name.error ? true : false}
                            variant="outlined"
                            label="Name"
                            value={name.value}
                            onChange={_name}
                            helperText={name.error}
                        />
                    </InputGrid>
                    <InputGrid item>
                        <TextField
                            disabled={isSubmitting}
                            error={email.error ? true : false}
                            variant="outlined"
                            label="Email"
                            value={email.value}
                            onChange={_email}
                            helperText={email.error}
                        />
                    </InputGrid>
                    <InputGrid item>
                        <TextField
                            disabled={isSubmitting}
                            error={password.error ? true : false}
                            variant="outlined"
                            label="Password"
                            type="password"
                            value={password.value}
                            onChange={_password}
                            helperText={password.error}
                        />
                    </InputGrid>
                    <InputGrid item>
                        <TextField
                            disabled={isSubmitting}
                            error={rePassword.error ? true : false}
                            variant="outlined"
                            label="Repeat password"
                            type="password"
                            value={rePassword.value}
                            onChange={_rePassword}
                            helperText={rePassword.error}
                        />
                    </InputGrid>
                    <InputGrid item>
                        <TextField
                            disabled={isSubmitting}
                            type="date"
                            varient="outlined"
                            label="Birthday"
                            defaultValue={dob}
                            InputLabelProps={{
                                shrink: true,
                            }}
                            onChange={event => setDob(event.target.value)}
                        />
                    </InputGrid>
                    <InputGrid item>
                        Gender:
                       <Radio 
                        disabled={isSubmitting}
                        checked={gender === 'FEMALE'} 
                        onChange={() => setGender('FEMALE')} />Female
                       <Radio disabled={isSubmitting} checked={gender === 'MALE'} onChange={() => setGender('MALE')} />Male
                       <Radio disabled={isSubmitting} checked={gender === 'OTHER'} onChange={() => setGender('OTHER')} />Other
                   </InputGrid>
                    <InputGrid item>
                        <Checkbox
                            disabled={isSubmitting}
                            value={aggreement}
                            onChange={() => setAggrement(!aggreement)}
                        />I agree to the terms
                   </InputGrid>
                    <InputGrid item>
                        <Button
                            disabled={hasErrors || isSubmitting}
                            variant="contained"
                            color="primary"
                            onClick={_register}
                        >Register</Button>
                    </InputGrid>
                </Grid>
            </Paper>
        </Container>
    )
}

export default RegisterPage