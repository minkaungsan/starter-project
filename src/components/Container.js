import Container from '@material-ui/core/Container'
import {styled} from '@material-ui/core/styles'

const MyContainer = styled(Container)({
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    minHeight: '100vh'
  })

export default MyContainer