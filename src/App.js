import React, { useContext, useState } from 'react';
import { makeStyles } from '@material-ui/core/styles'
import { useHistory, Switch, Route } from 'react-router-dom'
import LogInPage from './pages/LogInPage'
import HomePage from './pages/HomePage'
import RegisterPage from './pages/RegisterPage'
import ProfilePage from './pages/ProfilePage'
import AppBar from '@material-ui/core/AppBar'
import Toolbar from '@material-ui/core/Toolbar'
import IconButton from '@material-ui/core/IconButton'
import Typography from '@material-ui/core/Typography'
import Button from '@material-ui/core/Button'
import MenuIcon from '@material-ui/icons/Menu'
import Drawer from '@material-ui/core/Drawer'
import List from '@material-ui/core/List'
import ListItem from '@material-ui/core/ListItem'
import ListItemIcon from '@material-ui/core/ListItemIcon'
import ListItemText from '@material-ui/core/ListItemText'
import Divider from '@material-ui/core/Divider'
import Avatar from '@material-ui/core/Avatar'
import { LockOpen, Home, AccountBox, ExitToApp, HowToReg } from '@material-ui/icons'
import { deepOrange } from '@material-ui/core/colors'
import { GlobalContext } from './GlobalContext';

const drawerWidth = 220

const useStyles = makeStyles(theme => ({
  root: { display: 'flex' },
  appBar: { zIndex: theme.zIndex.drawer + 1 },
  drawer: { width: drawerWidth, flexShrink: 0 },
  drawerOpen: { width: drawerWidth },
  drawerClose: { width: 0},
  drawerContainer: { overflow: 'auto' },
  content: { flexGrow: 1, padding: theme.spacing(3) },
  title: { flexGrow: 1},
  menuButton: {  marginRight: theme.spacing(2) },
  avatarIcon: { 
    marginRight: theme.spacing(1),
    color: theme.palette.getContrastText(deepOrange[500]),
    backgroundColor: deepOrange[500],
  }
}))

function App() {
  const classes = useStyles()
  const history = useHistory()
  const { state: {user}, dispatch} = useContext(GlobalContext)

  const [drawerOpen, setDrawerOpen] = useState(true)

  const _logOut = () => {
    localStorage.removeItem('user')
    dispatch({ type: 'update-user', payload: null })
    history.push('/login')
  }

  return (
    <div className={"App " + classes.root}>
      <AppBar position="fixed" className={classes.appBar}>
        <Toolbar>
          <IconButton 
            className={classes.menuButton} 
            edge="start" color="inherit" 
            aria-label="menu" onClick={()=>setDrawerOpen(!drawerOpen)}>
            <MenuIcon />
          </IconButton>
          <Typography variant="h6" className={classes.title}>
            My App
          </Typography>
          {user !== null ? (
            <>
              <Avatar className={classes.avatarIcon}>{user.name.charAt(0).toUpperCase()}</Avatar>
              <Typography varient="h5">
                {user.name}
              </Typography>
            </>
          ) : (
            <Button
              variant="text"
              color="inherit"
              startIcon={<LockOpen/>}
              onClick={()=> history.push('/login')}
            >
              Log In
            </Button>
          )}
        </Toolbar>
      </AppBar>

      <Drawer
        className={classes.drawer}
        variant="permanent"
        classes={{
          paper: drawerOpen ? classes.drawerOpen : classes.drawerClose,
        }}
        
      >
        <Toolbar />
        <div className={classes.drawerContainer} >
          <List>
            <ListItem button onClick={()=> history.push('/')}>
              <ListItemIcon><Home /></ListItemIcon>
              <ListItemText primary="Home" />
            </ListItem>
            <ListItem button onClick={()=> history.push('/profile')}>
              <ListItemIcon><AccountBox /></ListItemIcon>
              <ListItemText primary="Profile" />
            </ListItem>
            <ListItem button onClick={()=>history.push('/register')}>
              <ListItemIcon><HowToReg /></ListItemIcon>
              <ListItemText primary="Register" />
            </ListItem>
          </List>
          <Divider />
          <List>
            {user !== null ? (
              <ListItem button onClick={_logOut}>
                <ListItemIcon><ExitToApp /></ListItemIcon>
                <ListItemText primary="Log Out" />
              </ListItem>
            ) : (
              <ListItem button onClick={()=> history.push('/login')}>
                <ListItemIcon><LockOpen /></ListItemIcon>
                <ListItemText primary="Log In" />
              </ListItem>
            )}
            
          </List>
        </div>
      </Drawer>
      <div className={classes.content}>
        <Toolbar/>
        <Switch>
          <Route exact path="/">
            <HomePage />
          </Route>
          <Route path="/login">
            <LogInPage />
          </Route>
          <Route path="/register">
            <RegisterPage />
          </Route>
          <Route path="/profile">
            <ProfilePage />
          </Route>
        </Switch>
      </div>
    </div>
  );
}

export default App;
