import React, {  useContext } from 'react'
import { Redirect } from 'react-router-dom'
import {GlobalContext} from '../GlobalContext'

const HomePage = () => {
    const { state: { user }, dispatch } = useContext(GlobalContext)

    return (
        <div>
            {user !== null ? (
                <>
                    <h1>Welcome home {user.name}</h1>
                    <ul>
                        <li>{user.email}</li>
                        <li>{user.gender}</li>
                        <li>{user.dob}</li>
                    </ul>
                </>
            ) : <Redirect to="/login" />
            }    
        </div>
        
    ) 
}

export default HomePage