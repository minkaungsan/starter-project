import React, { useState, useContext } from 'react'
import Grid from '@material-ui/core/Grid'
import InputGrid from '../components/InputGrid'
import TextField from '../components/TextField'
import Button from '@material-ui/core/Button'
import Checkbox from '@material-ui/core/Checkbox'
import { useHistory } from 'react-router-dom';
import Paper from '../components/Paper'
import Container from '../components/Container'
import axios from '../axios'
import {GlobalContext} from '../GlobalContext'

const LogInPage = () => {
  const { state, dispatch } = useContext(GlobalContext)

  const [email, setEmail] = useState({
    value: '',
    error: null
  })
  const [password, setPassword] = useState({
    value: '',
    error: null
  })

  const [showPassword, setShowPassword] = useState(false)

  const [isSubmitting, setIsSubmitting] = useState(false)

  const [logInError, setLogInError] = useState('')

  const history = useHistory()

  const _Email = event => {
    if (event.target.value === '') {
      setEmail({
        value: event.target.value,
        error: "Email can't be empty",
      })
    } else if (!(/^\w+([.-]?\w+)*@\w+([.-]?\w+)*(\.\w{2,3})+$/.test(event.target.value))) {
      setEmail({
        value: event.target.value,
        error: 'Please enter valid email'
      })
    } else {
      setEmail({
        value: event.target.value,
        error: null,
      })
    }
  }

  const _Password = event => {
    if (event.target.value === '') {
      setPassword({
        value: event.target.value,
        error: "Password can't be empty"
      })
    } else {
      setPassword({
        value: event.target.value,
        error: null
      })
    }
  }

  const _logIn = () => {
    setIsSubmitting(true)
    // api call
    axios.get(`/users?email=${email.value}&password=${password.value}`)
      .then(res => {
        if (res.data.length > 0) {
          // reroute to home page etc...
          // alert('log in successfully')
          localStorage.setItem('user', JSON.stringify(res.data[0]))
          dispatch({ type: 'update-user', payload: res.data[0]})
          history.push('/')
          setLogInError('')
          setIsSubmitting(false)
        } else {
          // alert('email or password wrong')
          setLogInError('wrong email or password.')
          setIsSubmitting(false)
          setPassword({ value: '', error: null })
        }
      })
      .catch(error => {
        setLogInError('plese check your internet connection')
        setIsSubmitting(false)
      })
  }

  const _register = () => {
    history.push('/register')
  }

  return (
    <Container>
      <Paper>
        <Grid
          container
          direction="column"
          alignItems="stretch"
        >
          <Grid item>
            <h3 style={{ textAlign: 'center', width: '100%' }}>Log In</h3>
          </Grid>
          <InputGrid item>
            <TextField
              disabled={isSubmitting}
              error={email.error ? true : false}
              variant="outlined"
              label="E-mail"
              value={email.value}
              onChange={_Email}
              helperText={email.error ? email.error : ''}
            />
          </InputGrid>
          <InputGrid item>
            <TextField
              disabled={isSubmitting}
              error={password.error ? true : false}
              type={showPassword ? 'text' : 'password'}
              variant="outlined"
              label="Password"
              value={password.value}
              onChange={_Password}
              helperText={password.error ? password.error : ''}
            />
            <Checkbox
              disabled={isSubmitting}
              value={showPassword}
              onChange={() => setShowPassword(!showPassword)}
            />
              show password
            </InputGrid>
          <Grid item style={{ marginLeft: '24px', color: 'red' }}>
            {logInError}
          </Grid>
          <Grid item style={{ margin: '10px 24px 24px 24px' }}>
            <Grid container justify="space-between">
              <Grid item>
                <Button variant="contained" color="primary" style={{ minWidth: '140px' }} onClick={_register}>Register</Button>
              </Grid>
              <Grid item>
                <Button
                  disabled={(email.error || password.error || isSubmitting) ? true : false}
                  variant="contained"
                  color="primary"
                  style={{ minWidth: '140px' }}
                  onClick={_logIn}
                >
                  Log In
                </Button>
              </Grid>
            </Grid>
          </Grid>
        </Grid>
      </Paper>
    </Container>
  )
}

export default LogInPage