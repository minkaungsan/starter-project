import React, { useContext, useState } from 'react'
import { Redirect } from 'react-router-dom'
import { GlobalContext } from '../GlobalContext'
import Grid from '@material-ui/core/Grid'
import InputGrid from '../components/InputGrid'
import TextField from '../components/TextField'
import Button from '@material-ui/core/Button'
import Radio from '@material-ui/core/Radio'
import axios from '../axios'

const ProfilePage = () => {
    const { state: { user }, dispatch } = useContext(GlobalContext)

    const [name, setName] = useState(user !== null ? user.name : '')
    const [email, setEmail] = useState(user !== null ? user.email : '')
    const [gender, setGender] = useState(user !== null ? user.gender : '')
    const [dob, setDob] = useState(user !== null ? user.dob : '')

    const _edit = () => {
        const editedUser = { name, email, gender, dob }
        // axios.delete(`http://localhost:5000/users/${user.id}`) example delete
        axios.patch(`/users/${user.id}`, editedUser)
            .then(res => {
                localStorage.setItem('user', JSON.stringify(res.data))
                dispatch({ type: 'update-user', payload: res.data})
            })
            .catch(error => console.log(error))
    }

    return (
        <>
            {user !== null ? (
                <Grid
                    container
                    direction="column"
                    alignItems="stretch"
                    style={{ maxWidth: '400px' }}
                >
                    <h2>Profile Page</h2>
                    <InputGrid item>
                        <TextField
                            variant="outlined"
                            label="Name"
                            value={name}
                            onChange={event => setName(event.target.value)} />
                    </InputGrid>
                    <InputGrid item>
                        <TextField
                            variant="outlined"
                            label="E-mail"
                            value={email}
                            onChange={event => setEmail(event.target.value)} />
                    </InputGrid>
                    <InputGrid item>
                        Gender:
                    <Radio
                            checked={gender === 'FEMALE'}
                            onChange={() => setGender('FEMALE')} />Female
                    <Radio
                            checked={gender === 'MALE'}
                            onChange={() => setGender('MALE')} />Male
                    <Radio
                            checked={gender === 'OTHER'}
                            onChange={() => setGender('OTHER')} />Other
                    </InputGrid>
                    <InputGrid item>
                        <TextField
                            variant="outlined"
                            type="date"
                            label="Date of Birth"
                            defaultValue={dob}
                            onChange={event => setDob(event.target.value)} />
                    </InputGrid>
                    <InputGrid item>
                        <Button variant="contained" color="primary" onClick={_edit}>Edit</Button>
                    </InputGrid>
                </Grid>
            ) : <Redirect to="/login" />}
        </>
    )
}

export default ProfilePage