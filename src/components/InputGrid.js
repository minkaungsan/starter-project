import Grid from '@material-ui/core/Grid'
import {styled} from '@material-ui/core/styles'

const InputGrid = styled(Grid)({
    margin: '14px',
    padding: '10px'
  })

export default InputGrid