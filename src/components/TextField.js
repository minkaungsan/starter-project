import TextField from '@material-ui/core/TextField'
import {styled} from '@material-ui/core/styles'

const MyTextField = styled(TextField)({
    width: '100%'
  })

export default MyTextField