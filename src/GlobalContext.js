import React, { createContext, useReducer } from 'react'

const user = localStorage.getItem('user')

const initialState = { user: user && JSON.parse(user) }

const GlobalContext = createContext(initialState)

const { Provider } = GlobalContext

const ContextProvider = ({ children }) => {
    const [state, dispatch] = useReducer( (state, action) => {
        switch(action.type) {
            case 'update-user':
                return { user: action.payload }
            default:
                return state
        }
    }, initialState)

    return <Provider value={{ state, dispatch }}>{children}</Provider>
}

export { GlobalContext, ContextProvider }